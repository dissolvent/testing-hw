export default {
    "items": [
        {
            "id": undefined,
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
        },
        {
            "id": undefined,
            "name": "Tvoluptatem",
            "price": 10.32,
            "quantity": 1
        },
        {
            "id": undefined,
            "name": "Scelerisque lacinia",
            "price": 18.9,
            "quantity": 1
        },
        {
            "id": undefined,
            "name": "Consectetur adipiscing",
            "price": 28.72,
            "quantity": 10
        },
        {
            "id": undefined,
            "name": "Condimentum aliquet",
            "price": 13.9,
            "quantity": 1
        }
    ],
    "total": 348.32
}