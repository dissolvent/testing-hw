import CartParser from './CartParser';
import * as uuid from 'uuid';
import expectedResult from '../samples/expectedOutputMock';

const cartCsvPath = `${__dirname}/../samples/cart.csv`;

jest.mock('uuid');

let parser, calcTotal;

beforeEach(() => {
	parser = new CartParser();
	calcTotal = parser.calcTotal;
	uuid.v4.mockReset();
});

describe('CartParser - unit tests', () => {
	it('should throw error when csv validation failed', () => {
		parser.readFile = jest.fn();
		parser.validate = jest.fn(() => [{
			type: 'header',
			row: 0,
			column: 0,
			message: 'Expected header to be named "Product name" but received Product.'
		}]);
		expect(() => parser.parse('')).toThrow('Validation failed!')
		expect(parser.readFile).toHaveBeenCalledTimes(1);
		expect(parser.validate).toHaveBeenCalledTimes(1);
	});

	it('should return items and total when csv correct', () => {
		parser.readFile = jest.fn(() => 'Product name,Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1');
		parser.validate = jest.fn(() => []);
		// should we mock this.parseLine and this.calcTotal for this test?
		expect(parser.parse()).toStrictEqual({ 
			items: [
				{
					"id": undefined,
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
			},
			{
					"id": undefined,
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
			}
		],
		total: 28.32
	});
		expect(parser.readFile).toHaveBeenCalledTimes(1);
		expect(parser.validate).toHaveBeenCalledTimes(1);
	});

	it('should return error when header name incorrect', () => { 
		expect(parser.validate('Product,Price,Quantity')).toStrictEqual([{
			type: 'header',
			row: 0,
			column: 0,
			message: 'Expected header to be named "Product name" but received Product.'
		}]);
	});

	it('should return error when product fields count incorrect', () => {
		const data = 'Product name,Price,Quantity\nMollis consequat,9.00\n';
		expect(parser.validate(data)).toStrictEqual([{
			type: 'row',
			row: 1,
			column: -1,
			message: 'Expected row to have 3 cells but received 2.'
		}]);
	});

	it('should return error when product name is empty', () => {
		const data = 'Product name,Price,Quantity\n,2,9.00\n';
		expect(parser.validate(data)).toStrictEqual([{
			type: 'cell',
			row: 1,
			column: 0,
			message: 'Expected cell to be a nonempty string but received "".'
		}]);
	});

	it('should return error when product price contains not positive number', () => {
		const data = 'Product name,Price,Quantity\nMollis consequat,-2,2\n';
		expect(parser.validate(data)).toStrictEqual([{
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "-2".'
		}]);
	});

	it('should return error when product price is NaN', () => {
		const data = 'Product name,Price,Quantity\nMollis consequat," ",2\n';
		expect(parser.validate(data)).toStrictEqual([{
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "" "".'
		}]);
	});

	it('should return error when product quantity is NaN', () => {
		const data = 'Product name,Price,Quantity\nMollis consequat,10,NaN\n';
		expect(parser.validate(data)).toStrictEqual([{
			type: 'cell',
			row: 1,
			column: 2,
			message: 'Expected cell to be a positive number but received "NaN".'
		}]);
	});

	it('should return empty array when all fields are valid', () => {
		const data = 'Product name,Price,Quantity\nMollis consequat,2,2\n';
		expect(parser.validate(data)).toStrictEqual([]);
	});

	it('should return item when parsed csv line', () => {
		expect(parser.parseLine('Scelerisque lacinia,18.90,1')).toStrictEqual({
			id: undefined,
			name: 'Scelerisque lacinia',
			price: 18.90,
			'quantity': 1
		})
	})

	it('should calculate total price of items', () => {
		const cartItems = [{
			"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
			"name": "Mollis consequat",
			"price": 9,
			"quantity": 2
		},
		{
			"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
			"name": "Tvoluptatem",
			"price": 10.32,
			"quantity": 1
		}];
		expect(calcTotal(cartItems)).toBeCloseTo(28.32, 2);
	})
});

describe('CartParser - integration test', () => {
	it('can read csv file and parse it', () => {
		expect(parser.parse(cartCsvPath)).toStrictEqual(expectedResult);
	})
});
